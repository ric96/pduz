/*
 * Copyright (c) 2016 Open-RnD Sp. z o.o.
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/zephyr.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/util.h>
#include <zephyr/sys/printk.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/usb/usb_device.h>
#include <zephyr/devicetree.h>
#include <zephyr/console/console.h>
#include <inttypes.h>
#include <stdlib.h>

#define ZEPHYR_USER_NODE DT_PATH(zephyr_user)

BUILD_ASSERT(DT_NODE_HAS_COMPAT(DT_CHOSEN(zephyr_console), zephyr_cdc_acm_uart),
	     "Console device is not ACM CDC UART device");


#define SLEEP_TIME_MS	1
#define FUNC_MASK 0x80
#define RELAY_MASK 0x7F


static struct gpio_dt_spec relaygpio[32] = 
{
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 0),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 1),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 2),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 3),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 4),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 5),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 6),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 7),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 8),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 9),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 10),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 11),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 12),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 13),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 14),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 15),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 16),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 17),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 18),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 19),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 20),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 21),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 22),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 23),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 24),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 25),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 26),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 27),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 28),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 29),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 30),
	GPIO_DT_SPEC_GET_BY_IDX(ZEPHYR_USER_NODE, relaygpios, 31)

};


void main(void)
{
	int ret = 0;
	const struct device *dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_console));
	uint32_t dtr = 0;
	uint8_t data, relay, function;

	if (usb_enable(NULL)) {
		return;
	}

	while (!dtr) {
		uart_line_ctrl_get(dev, UART_LINE_CTRL_DTR, &dtr);
		/* Give CPU resources to low priority threads. */
		k_sleep(K_MSEC(100));
	}

	console_init();

	for(int i = 0; i < 32; i++)
	{
		if (relaygpio[i].port && !device_is_ready(relaygpio[i].port)) {
			printk("Error %d: RELAY GPIO device %s is not ready; ignoring it\n",
				ret, relaygpio[i].port->name);
			relaygpio[i].port = NULL;
		}

		if (relaygpio[i].port) {
		ret = gpio_pin_configure_dt(&relaygpio[i], GPIO_OUTPUT_HIGH);
		if (ret != 0) {
			printk("Error %d: failed to configure LED device %s pin %d\n",
			       ret, relaygpio[i].port->name, relaygpio[i].pin);
			relaygpio[i].port = NULL;
		} else {
			printk("Set up LED at %s pin %d\n", relaygpio[i].port->name, relaygpio[i].pin);
		}
	}
	}

	while (1) {
		
		data = console_getchar();

		function = data & FUNC_MASK;
		relay = data & RELAY_MASK;

		printk("data: [0x%x] %d\n", data, data);
		printk("function: [0x%x] %d\n", function, function);
		printk("relay: [0x%x] %d\n", relay, relay);

		if(relay < 32)
			gpio_pin_set_dt(&relaygpio[relay],(function && FUNC_MASK));


		//k_msleep(100);

	}

}
