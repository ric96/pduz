#!/usr/bin/env python

import sys
import serial
import time
from serial.tools import list_ports

# Get usb device serial number as first argument and use it to figure out device path string
ser = serial.Serial(list(list_ports.grep(sys.argv[1]))[0].device, 115200, timeout=1)

if sys.argv[2] == "test":
    while True:
        for i in range(32):
            data = 0 + (i)
            data_char = chr(data)
            print(hex(data))
            ser.write(data_char.encode())
            time.sleep(int(sys.argv[3]))
            data = 128 + (i)
            data_char = chr(data)
            print(hex(data))
            ser.write(data_char.encode())

else:
    relay = sys.argv[2]

    if sys.argv[3] == "on":
        data = 128 + (int(relay)-1) # decrement relay to compensate for relay count in pdu starting from 0
        data_char = chr(data)
        print(hex(data))
        ser.write(data_char.encode())

    elif sys.argv[3] == "off":
        data = 0 + (int(relay)-1) # decrement relay to compensate for relay count in pdu starting from 0
        data_char = chr(data)
        print(hex(data))
        ser.write(data_char.encode())

    elif sys.argv[3] == "reset":
        data = 0 + (int(relay)-1) # decrement relay to compensate for relay count in pdu starting from 0
        data_char = chr(data)
        print(hex(data))
        ser.write(data_char.encode())
        time.sleep(5)
        data = 128 + (int(relay)-1) # decrement relay to compensate for relay count in pdu starting from 0
        data_char = chr(data)
        print(hex(data))
        ser.write(data_char.encode())

ser.close()
